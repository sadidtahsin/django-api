from django.urls import path
from .views import article_list,article_detail,ArticleAPIView,ArticaleDetailView,GenericAPIView
urlpatterns = [
    path('article_api/',ArticleAPIView.as_view()),
    path('article_details_api/<int:id>',ArticaleDetailView.as_view()),
    path('article/',article_list),
    path('generic/article/<int:id>',GenericAPIView.as_view()),
    path('generic/article/',GenericAPIView.as_view()),
    path('detail/<int:pk>',article_detail)
]
